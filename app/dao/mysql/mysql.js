/**
 * Created by Mic on 16/1/23.
 */

var sqlclient = module.exports;

var _pool;

var NND = {};

NND.init = function (app) {
    _pool = require('./dao-pool').createMysqlPool(app);
};

NND.query = function (sql, args, cb) {
    // _pool.acquire(function (err, client) {
    //     if (!!err) {
    //         console.log('[sqlqueryErr] ' + err.stack);
    //         return;
    //     }
    //     client.query(sql, args, function (err, res) {
    //         _pool.release(client);
    //         cb(err, res);
    //     });
    // });
    _pool.acquire().then(function (client) {
        client.query(sql,args,function (err,res) {
            _pool.release(client);
            cb(err,res);
        });
    });
};

NND.shutdown = function (app) {
    _pool.destroyAllNow(app);
};

sqlclient.init = function (app) {
    if (!!_pool) {
        return sqlclient;
    } else {
        NND.init(app);
        sqlclient.insert = NND.query;
        sqlclient.update = NND.query;
        sqlclient.delete = NND.query;
        sqlclient.query = NND.query;
        return sqlclient;
    }
};

sqlclient.shutdown = function (app) {
    NND.shutdown(app);
};