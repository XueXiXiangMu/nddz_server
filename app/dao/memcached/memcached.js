/**
 * Created by Mic on 16/1/11.
 */

var cachedClient = module.exports;
var _mem;

cachedClient.init = function (app) {
    if (!!_mem) {
        return cachedClient;
    } else {
        var M = require('memcached');
        _mem = new M(app.get("memcached"), {compressionThreshold: 10});
        return cachedClient;
    }
};

cachedClient.set = function (key, value, cb) {
    _mem.set(key, value, 0, function (err, result) {
        if (err) {
            console.error(err);
        }
        if (!!cb && typeof cb === 'function') {
            cb(err, result);
        }
    });
};
cachedClient.add = function (key, value, cb) {
    _mem.add(key, value, 0, function (err, result) {
        if (err) {
            console.error(err);
        }
        if (!!cb && typeof cb === 'function') {
            cb(err, result);
        }
    });
};
cachedClient.replace = function (key, value, cb) {
    _mem.replace(key, value, 0, function (err, result) {
        if (err) {
            console.error(err);
        }
        if (!!cb && typeof cb === 'function') {
            cb(err, result);
        }
    });
};

cachedClient.get = function (key, cb) {
    _mem.get(key, function (err, result) {
        if (err) {
            console.error(err);
        }
        if (!!cb && typeof cb === 'function') {
            cb(err, result);
        }
    });
};

cachedClient.gets = function (key, cb) {
    _mem.gets(key, function (err, result) {
        if (err) {
            console.error(err);
        }
        if (!!cb && typeof cb === 'function') {
            cb(err, result);
        }
    });
};

cachedClient.del = function (key, cb) {
    _mem.del(key, function (err, result) {
        if (err) {
            console.error(err);
        }
        if (!!cb && typeof cb === 'function') {
            cb(err, result);
        }
    });
};
cachedClient.append = function (key, value, cb) {
    _mem.append(key, value, function (err, result) {
        if (err) {
            console.error(err);
        }
        if (!!cb && typeof cb === 'function') {
            cb(err, result);
        }
    });
};
cachedClient.prepend = function (key, value, cb) {
    _mem.prepend(key, value, function (err, result) {
        if (err) {
            console.error(err);
        }
        if (!!cb && typeof cb === 'function') {
            cb(err, result);
        }
    });
};