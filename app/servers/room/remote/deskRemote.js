/**
 * Created by Mic on 16/2/22.
 */
var logger = require('pomelo-logger').getLogger(__filename);
var pomelo = require('pomelo');
var mem = pomelo.app.get('memclient');

var DeskRemote = module.exports;

DeskRemote.test = function (msg, next) {
    console.log('deskRemote test', msg);
    next();
};
DeskRemote.ready = function (msg, next) {
    console.log('ready', msg);
    next();
};
DeskRemote.jiao = function (msg, next) {
    var player = msg.player;
    var desk = getDeskByPlayer(player);

    desk.jiaoResult(player.seatIndex, msg.value);
    next();
};
DeskRemote.qiang = function (msg, next) {
    var player = msg.player;
    var desk = getDeskByPlayer(player);

    desk.qiangResult(player.seatIndex, msg.value);
    next();
};
DeskRemote.double = function (msg, next) {
    var player = msg.player;
    var desk = getDeskByPlayer(player);

    desk.doubleResult(player.seatIndex, msg.value);
    next();
};
DeskRemote.play = function (msg, next) {
    //console.warn(pomelo.app.room);
    var player = msg.player;
    var desk = getDeskByPlayer(player);

    desk.playResult(player.seatIndex, msg.value);
    next();
};
//托管
DeskRemote.hosting = function (msg, next) {
    var player = msg.player;
    var desk = getDeskByPlayer(player);

    desk.hosting(player);
    next();
};

DeskRemote.unHosting = function (msg, next) {
    var player = msg.player;
    var desk = getDeskByPlayer(player);

    desk.unHosting(player);
    next();
};
//开始游戏了把桌子信息存入mem
DeskRemote.setDeskMem = function (uuid, msg, next) {
    mem.get(uuid, function (err, data) {
        if (data) {
            data['deskName'] = msg;
            mem.set(uuid, data, function (err, data) {
                next(data);
            });
        } else {
            next(null);
        }
    });
};
//桌子解散了把桌子信息删除从mem 需要删除吗 如果进入下一场直接覆盖得了 好 那就覆盖了
DeskRemote.deleteDeskMem = function (msg, next) {
    logger.info('准备update删除deskInfo');

};

function getDeskByPlayer(player) {
    var room = pomelo.app.room;
    return room.findDeskByName(player.deskName);
}