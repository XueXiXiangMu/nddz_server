/**
 * Created by Mic on 16/2/22.
 */
var logger = require('pomelo-logger').getLogger(__filename);
var pomelo = require('pomelo');
//var userDao = require('../../../dao/userDao');
var consts = require('../../../consts/consts');
var mem = pomelo.app.get('memclient');

var handler = module.exports;

//快速加入
handler.quickJoin = function (msg, session, next) {
    //是否之前中途退出游戏
    var self = this;
    checkPlayer(msg.player.uuid, function (data) {
        var sid, serverId;
        if (!!data) {
            logger.fatal('你中途退出游戏了,请回到桌子-> ' + data);
            serverId = data.serverId;
            sid = session.get('sid');
            session.set('serverId', serverId);
            session.pushAll();
            pomelo.app.rpc.room.roomRemote.backDesk(session, msg.player, sid, data.deskName, next);

        } else {
            var money = msg.player.money;
            sid = session.get('sid');
            serverId = self.checkMoney(money);
            session.set('serverId', serverId);
            session.pushAll();

            if (!!serverId) {
                pomelo.app.rpc.room.roomRemote.enterRoom(session, msg.player, sid, next);
            } else {
                next(null, {
                    code: 500,
                    msg: '金币不足,请去商城充值'
                });
            }
        }
    });
};
//进入选定的场次
handler.enterRoom = function (msg, session, next) {
    logger.fatal('进入游戏场 ', msg);
    var roomName = msg.roomName;
    var player = msg.player;
    var self = this;
    //检查是否在游戏中
    checkPlayer(player.uuid, function (data) {
        var sid, serverId;
        if (!!data) {
            logger.fatal('你中途退出游戏了,请回到桌子-> ' + data);
            serverId = data.serverId;
            sid = session.get('sid');
            session.set('serverId', serverId);
            session.pushAll();
            pomelo.app.rpc.room.roomRemote.backDesk(session, msg.player, sid, data.deskName, next);
        } else {
            self.checkEnterRoom(roomName, player.money, function (err, data) {
                var code = data.code;
                if (code == 500) {
                    next(null, data);
                } else {
                    var sid = session.get('sid');
                    session.set('serverId', data.serverId);
                    session.pushAll();
                    pomelo.app.rpc.room.roomRemote.enterRoom(session, msg.player, sid, next);
                }
            });
        }
    });

};

handler.checkEnterRoom = function (roomName, money, next) {
    var msg = null;
    money = Number(money);

    if (money < 1000) {
        msg = 1;
    }

    switch (roomName) {
        case 'normal_room1':
            if (money >= 1000) {
                next(null, {
                    code: consts.Code.Ok,
                    serverId: consts.Room.Normal.Room1.serverId
                });
            }
            break;
        case 'normal_room2':
            if (money < 100000) {
                msg = 1;
            } else {
                next(null, {
                    code: consts.Code.Ok,
                    serverId: consts.Room.Normal.Room2.serverId
                });

            }
            break;
        case 'normal_room3':
            if (money < 500000) {
                msg = 1;
            } else {
                next(null, {
                    code: consts.Code.Ok,
                    serverId: consts.Room.Normal.Room3.serverId
                });
            }
            break;
        default:
            msg = 0;
            break;
    }
    if (msg != null) {
        next(null, {
            code: 500,
            msg: msg
        });
    }
};

handler.updatePlayerInfo = function (msg, session, next) {
    var sid = session.get('sid');
    var playerInfo = msg.playerInfo;

    logger.info('updatePlayerInfo');
    //userDao.getUserByUuid(playerInfo, function (err, res) {
    //pomelo.rpc.interface.interfaceRemote.getUserBySid(playerInfo.uuid, function (err,de,res) {
    //    if (!!err) {
    //        logger.error('update User for userDao failed ' + err.stack);
    //        next(null, {
    //            keyCode: 500
    //        });
    //    } else {
    //        playerInfo.money = res[0].money;
    //        next(null, {
    //            keyCode: 200,
    //            player: playerInfo
    //        });
    //    }
    //});
};
handler.purchase = function (msg, session, next) {
    var sid = session.get('sid');
    var num = msg.num;
    var playerInfo = msg.playerInfo;

    //userDao.updateMoney(playerInfo, num, function (err, res) {
    //    if (!!err) {
    //        logger.error('购买失败 ' + err.stack);
    //        next(null, {
    //            keyCode: 500,
    //            player: playerInfo
    //        });
    //    } else {
    //        playerInfo.money = parseInt(playerInfo.money) + num;
    //        console.log('购买成功 ', playerInfo);
    //        next(null, {
    //            keyCode: 200,
    //            player: playerInfo
    //        });
    //    }
    //});
    //pomelo.rpc.interface.interfaceRemote.purchase();
};

handler.checkMoney = function (money) {
    var msg = null;
    if (money < 1000) {
        msg = null;
    } else if (money >= 1000 && money < 100000) {  //初级场
        msg = consts.Room.Normal.Room1.serverId;
    } else if (money >= 100000 && money < 500000) {  //中级场
        msg = consts.Room.Normal.Room2.serverId;
    } else if (money >= 500000) {   //高级场
        msg = consts.Room.Normal.Room3.serverId;
    }
    return msg;
};

var checkPlayer = function (uuid, cb) {
    mem.get(uuid, function (err, data) {
        cb(data);
    });
};